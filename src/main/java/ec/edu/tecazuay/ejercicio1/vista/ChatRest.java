/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.ChatRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Chat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Peralta y Monje
 */
@RestController
@RequestMapping("/chat")
public class ChatRest {

    @Autowired
    ChatRepositorio chatRepositorio;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<Chat> listarChat() {
        return chatRepositorio.findAll();
    }
//para todos
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Chat registrarChat(@RequestBody Chat p) {
        return chatRepositorio.save(p);
    }
    
    @RequestMapping(value = "/{codChat}", method = RequestMethod.GET)
    @ResponseBody
    public Chat buscarChat(@PathVariable Long codChat) {
        return chatRepositorio.getOne(codChat);
    }
    @RequestMapping(value = "/{codChat}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrarChat(@PathVariable Long codChat) {
        chatRepositorio.deleteById(codChat);
    }
}
