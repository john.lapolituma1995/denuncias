/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.PerfilRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Perfil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Peralta y Monje
 */
@RestController
@RequestMapping("/perfil")
public class PerfilRest {

    @Autowired
    PerfilRepositorio perfilRepositorio;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<Perfil> listarPerfil() {
        return perfilRepositorio.findAll();
    }
//para todos
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Perfil registrarPerfil(@RequestBody Perfil p) {
        return perfilRepositorio.save(p);
    }
    
    @RequestMapping(value = "/{codPerfil}", method = RequestMethod.GET)
    @ResponseBody
    public Perfil buscarPerfil(@PathVariable Long codPerfil) {
        return perfilRepositorio.getOne(codPerfil);
    }
    @RequestMapping(value = "/{codPerfil}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrarPerfil(@PathVariable Long codPerfil) {
        perfilRepositorio.deleteById(codPerfil);
    }
}
