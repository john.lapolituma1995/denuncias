/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.DenunciaRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Denuncia;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Peralta y Monje
 */
@RestController
@RequestMapping("/denuncia")
public class DenunciaRest {

    @Autowired
    DenunciaRepositorio denunciaRepositorio;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<Denuncia> listarDenuncia() {
        return denunciaRepositorio.findAll();
    }
//para todos
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Denuncia registrarDenuncia(@RequestBody Denuncia p) {
        return denunciaRepositorio.save(p);
    }
    
    @RequestMapping(value = "/{codDenuncia}", method = RequestMethod.GET)
    @ResponseBody
    public Denuncia buscarDenuncia(@PathVariable Long codDenuncia) {
        return denunciaRepositorio.getOne(codDenuncia);
    }
    @RequestMapping(value = "/{codDenuncia}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrarDenuncia(@PathVariable Long codDenuncia) {
        denunciaRepositorio.deleteById(codDenuncia);
    }
}
