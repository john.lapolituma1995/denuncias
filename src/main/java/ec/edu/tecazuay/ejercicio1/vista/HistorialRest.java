/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.HistorialRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Historial;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Peralta y Monje
 */
@RestController
@RequestMapping("/historial")
public class HistorialRest {

    @Autowired
    HistorialRepositorio historialRepositorio;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public List<Historial> listarHistorial() {
        return historialRepositorio.findAll();
    }
//para todos
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Historial registrarHistorial(@RequestBody Historial p) {
        return historialRepositorio.save(p);
    }
    
    @RequestMapping(value = "/{codHistorial}", method = RequestMethod.GET)
    @ResponseBody
    public Historial buscarHistorial(@PathVariable Long codHistorial) {
        return historialRepositorio.getOne(codHistorial);
    }
    @RequestMapping(value = "/{codHistorial}", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void borrarHistorial(@PathVariable Long codHistorial) {
        historialRepositorio.deleteById(codHistorial);
    }
}
