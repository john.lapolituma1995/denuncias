/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Denuncia;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Peralta y Monje
 */
public interface DenunciaRepositorio extends JpaRepository<Denuncia, Long>{
    
}
