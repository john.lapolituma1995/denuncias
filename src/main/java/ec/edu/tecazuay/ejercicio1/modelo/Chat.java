/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.tecazuay.ejercicio1.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author PCX
 */
@Entity
public class Chat implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codChat;
    private  String conversacion;
    private Date fechaHoraConversacion;
    
    @OneToMany
    List <Persona>persona;
    
    @OneToOne
    Historial historial;

    public Long getCodChat() {
        return codChat;
    }

    public void setCodChat(Long codChat) {
        this.codChat = codChat;
    }

    public String getConversacion() {
        return conversacion;
    }

    public void setConversacion(String conversacion) {
        this.conversacion = conversacion;
    }

    public Date getFechaHoraConversacion() {
        return fechaHoraConversacion;
    }

    public void setFechaHoraConversacion(Date fechaHoraConversacion) {
        this.fechaHoraConversacion = fechaHoraConversacion;
    }

    public List<Persona> getPersona() {
        return persona;
    }

    public void setPersona(List<Persona> persona) {
        this.persona = persona;
    }

    public Historial getHistorial() {
        return historial;
    }

    public void setHistorial(Historial historial) {
        this.historial = historial;
    }
    
}
